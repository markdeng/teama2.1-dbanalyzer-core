package com.db.grad.teama21;

public class Account {
    private double  itsBalance = 20;
    
    public  Account()
    {
    }
     
    public  Account( double amt )
    {
        itsBalance = amt;
    }
     
    public  double  getBalance()
    {
        return itsBalance;
    }
     
    public  double  credit( double amt )
    {
        itsBalance += amt;
         
        return itsBalance;
    }
     
    public  double  dedit( double amt )
    {
        if( (itsBalance - amt) >= 0 )
            itsBalance += amt;
         
        return itsBalance;
    }
}
